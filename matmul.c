#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>


#ifdef OPENMP
inicio = omp get wtime ();
#endif

#define RAND rand() % 100


void init_mat_sup (int dim, float *M);
void init_mat_inf (int dim, float *M);
void init_mat (int dim, float *M);
void matmulSin (float *A, float *B, float *C, int dim);
void matmulCon (float *A, float *B, float *C, int dim);
void matmul_supSin (float *A, float *B, float *C, int dim);
void matmul_supCon (float *A, float *B, float *C, int dim);
void matmul_inf (float *A, float *B, float *C, int dim);
void print_matrix (float *M, int dim, int numMatrix);

/* Usage: ./matmul <dim> [block_size]*/


int main (int argc, char* argv[])
{
	int block_size = 1, dim;
	float *A, *B, *C;


	dim = atoi (argv[1]);
	if (argc == 3) block_size = atoi (argv[2]);

	float matrix1[dim][dim], matrix2[dim][dim], matrixResCon[dim][dim], matrixResSin[dim][dim];
	init_mat(dim, &matrix1[0][0]);
	init_mat(dim, &matrix2[0][0]);

	//	print_matrix(&matrix1[0][0], dim, 1);
	//	print_matrix(&matrix2[0][0], dim, 2);

	matmulSin(&matrix1[0][0], &matrix2[0][0], &matrixResSin[0][0], dim);
	matmulCon(&matrix1[0][0], &matrix2[0][0], &matrixResCon[0][0], dim);

	//	matmul_supCon
	//	matmul_supSin

	//print_matrix(&matrix1[0][0], dim, 1);
	//print_matrix(&matrix2[0][0], dim, 2);
	print_matrix(&matrixResCon[0][0], dim, 99);
	printf("-----------\n" );
	print_matrix(&matrixResSin[0][0], dim, 66);

	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void init_mat (int dim, float *M)
{
	int i,j,m,n,k;
	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			M[i*dim+j] = i+1+j;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void matmulSin (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	for (k=0; k < dim; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void matmulCon (float *A, float *B, float *C, int dim){
	int i, j, k;

	#pragma omp parallel num_threads(10) private(i,j) shared(A,B,C)
	#pragma omp for
	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	#pragma omp parallel num_threads(10) private(i,j,k) shared(A,B,C)
	#pragma omp for
	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	for (k=0; k < dim; k++){
		C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
		//printf("%f = %f x %f\n", C[i*dim+j], A[i*dim+k] , B[j+k*dim]);
	}
}


void matmul_supCon (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	#pragma omp parallel
	{
		#pragma omp for schedule(dynamic)
		for (i=0; i < dim; i++){

			for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

			for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
			for (k=(i+1); k < dim; k++)
			C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
		}
	}
}

void matmul_supSin (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=0; i < (dim-1); i++)
	for (j=0; j < (dim-1); j++)
	for (k=(i+1); k < dim; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
	for (j=1; j < dim; j++)
	for (k=0; k < i; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void print_matrix (float *M, int dim, int numMatrix){
	for(int i=0; i<dim; i++){
		for(int u=0; u<dim; u++){
			printf("|%d| Matrix[%d][%d] = [%lf]\n",numMatrix, i, u, *M );
			M++;
		}
	}
}
